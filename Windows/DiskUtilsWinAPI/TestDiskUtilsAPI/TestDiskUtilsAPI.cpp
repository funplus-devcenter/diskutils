﻿// TestDiskUtilsAPI.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "DiskUtilsAPI.h"
#include <vector>

DISKUTILS_API int getAvailableDiskSpace(char* drive);
DISKUTILS_API int getTotalDiskSpace(char* drive);
DISKUTILS_API int getBusyDiskSpace(char* drive);
DISKUTILS_API int getAllDrivers(vector<char*>* drivers);


struct AA
{
	int a;

	short b;

	double c;

	int d;
};

#pragma pack(2)
struct BB
{
	int a;

	short b;

	double c;

	int d;
};

int main()
{

	std::cout << "AA offsetof a : " << offsetof(AA,a) << std::endl;
	std::cout << "AA offsetof b : " << offsetof(AA, b) << std::endl;
	std::cout << "AA offsetof c : " << offsetof(AA, c) << std::endl;
	std::cout << "AA offsetof d : " << offsetof(AA, d) << std::endl;


	std::cout << "BB offsetof a : " << offsetof(BB, a) << std::endl;
	std::cout << "BB offsetof b : " << offsetof(BB, b) << std::endl;
	std::cout << "BB offsetof c : " << offsetof(BB, c) << std::endl;
	std::cout << "BB offsetof d : " << offsetof(BB, d) << std::endl;

	
	std::vector<char*>* dirvers = new std::vector<char*>();
	int result = getAllDrivers(dirvers);

	std::cout << "The sizse of colorref is : " << sizeof(COLORREF) << std::endl;
	
	if(result == 0)
	{
		std::cout << "Get drivers failures" << std::endl;
		return 1;
	}
	else
	{
		for (size_t i = 0; i < result ; i++)
		{
			auto dirve = (*dirvers)[i];
			std::cout << "Direve Name : " << dirve<< std::endl;
			try
			{
				int availableDiskSpace = getAvailableDiskSpace(dirve);
				int totalDiskSpace = getTotalDiskSpace(dirve);
				int busydiskspace = getBusyDiskSpace(dirve);

				std::cout << "AvailableDiskSpace : " << availableDiskSpace << "M ." << endl;
				std::cout << "TotalDiskSpace : " << totalDiskSpace << "M ." << endl;
				std::cout << "Busydiskspace: " << busydiskspace << "M ." << endl;

				std::cout << endl;
			}
			catch (const std::exception& e)
			{
			std:cout << "Error : " << e.what() << endl;
			}
		}

		
	}

	/*for (auto i = dirvers->begin(); i != dirvers->end(); ++i)
	{
		delete* i;
	}*/

	delete dirvers;
	
	
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
