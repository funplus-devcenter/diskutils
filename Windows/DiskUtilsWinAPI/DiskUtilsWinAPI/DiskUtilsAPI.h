#pragma once

#include "HardDiskManager.h"
#include <Windows.h>
#include <cstdio>
#include <vector>
using namespace std;

#ifdef BUILD_DLL
#define DISKUTILS_API __declspec(dllexport)
#else
#define DISKUTILS_API __declspec(dllimport)
#endif




extern "C"
{
	DISKUTILS_API int getAvailableDiskSpace(char* drive);
	DISKUTILS_API int getTotalDiskSpace(char* drive);
	DISKUTILS_API int getBusyDiskSpace(char* drive);
	DISKUTILS_API int getAllDrivers(vector<char*>* drivers);
}

class DiskUtilsAPI
{
public:
	DiskUtilsAPI();
	~DiskUtilsAPI();
};


