#include "DiskUtilsAPI.h"
#include <vector>
#include <iostream>


DISKUTILS_API int getAvailableDiskSpace(char* drive)
{
	PULARGE_INTEGER _available = new ULARGE_INTEGER;
	PULARGE_INTEGER _total = new ULARGE_INTEGER;
	PULARGE_INTEGER _free = new ULARGE_INTEGER;

	GetDiskFreeSpaceEx(drive, _available, _total, _free);

	DWORD64 MEGA_BYTE = 1048576;

	DWORD64 free = _available->QuadPart / MEGA_BYTE;

	char s[12];
	sprintf_s(s, "%llu", free);

	int ret;
	sscanf_s(s, "%d", &ret);

	delete(_available);
	delete(_total);
	delete(_free);

	return ret;
}


DISKUTILS_API int getTotalDiskSpace(char* drive)
{
	PULARGE_INTEGER _available = new ULARGE_INTEGER;
	PULARGE_INTEGER _total = new ULARGE_INTEGER;
	PULARGE_INTEGER _free = new ULARGE_INTEGER;

	GetDiskFreeSpaceEx(drive, _available, _total, _free);

	DWORD64 MEGA_BYTE = 1048576;

	DWORD64 total = _total->QuadPart / MEGA_BYTE;

	char s[12];
	sprintf_s(s, "%llu", total);

	int ret;
	sscanf_s(s, "%d", &ret);

	delete(_available);
	delete(_total);
	delete(_free);

	return ret;
}


DISKUTILS_API int getBusyDiskSpace(char* drive)
{
	PULARGE_INTEGER _available = new ULARGE_INTEGER;
	PULARGE_INTEGER _total = new ULARGE_INTEGER;
	PULARGE_INTEGER _free = new ULARGE_INTEGER;

	GetDiskFreeSpaceEx(drive, _available, _total, _free);

	DWORD64 MEGA_BYTE = 1048576;

	DWORD64 busy = (_total->QuadPart - _free->QuadPart) / MEGA_BYTE;

	char s[12];
	sprintf_s(s, "%llu", busy);

	int ret;
	sscanf_s(s, "%d", &ret);

	delete(_available);
	delete(_total);
	delete(_free);

	return ret;
}

DISKUTILS_API int getAllDrivers(vector<char*>* drivers)
{
	DWORD dwSize = MAX_PATH;
	char szLogicDrivers[MAX_PATH] = { 0 };
	DWORD dwResult = GetLogicalDriveStrings(dwSize,szLogicDrivers);

	int driversCount = 0;
	if(dwResult > 0 && dwResult <= MAX_PATH)
	{
		char* szSingleDrive = szLogicDrivers;

		while (*szSingleDrive)
		{
			//std::cout <<"Drive: %s\n"<< szSingleDrive << std::endl;
			
			/*std::cout<<*/
			/*char* result = &szSingleDrive[0];

			std::cout << "result : " << result << std::endl;*/

			auto charCount = strlen(szSingleDrive) + 1;
			char* c = new char[charCount];

			for (size_t i = 0; i < charCount; i++)
			{
				c[i] = *(szSingleDrive + i);
			}
			
			drivers->push_back(c);
			driversCount++;
			
			szSingleDrive += strlen(szSingleDrive) + 1;
		}
	}

	std::cout << "drivers count : " << driversCount << std::endl;
	
	
	return driversCount;
}



DiskUtilsAPI::DiskUtilsAPI()
{
}


DiskUtilsAPI::~DiskUtilsAPI()
{
}
